from multiprocessing import cpu_count
from os import environ

workers = cpu_count()
bind = '0.0.0.0:8000'
reload = True

if environ.get('DEBUG'):
    capture_output = True
    enable_stdio_inheritance = True
    loglevel = 'debug'
