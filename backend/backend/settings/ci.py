from .base import *

SECRET_KEY = "H3hGZ9ErGfzF2Nuvsp8u"
DEBUG = False
ALLOWED_HOSTS = ['*']

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "HOST": "localhost",
        "PORT": 5432,
        "NAME": "edtemple",
        "USER": "edtemple",
        "PASSWORD": "H3hGZ9ErGfzF2Nuvsp8u",
    }
}
