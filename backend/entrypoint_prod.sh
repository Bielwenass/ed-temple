#!/usr/bin/env bash
python manage.py makemigrations --settings=backend.settings.docker --no-input
python manage.py migrate --settings=backend.settings.docker --no-input
python manage.py collectstatic --settings=backend.settings.docker --no-input
gunicorn backend.wsgi -c gunicorn_conf.py