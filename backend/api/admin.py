from django.contrib import admin

from api.models import User, Question, Topic, Student, Subject, Group, Answer, StudentTopic, Teacher, StudentAnswer


class AnswerInLine(admin.StackedInline):
    model = Answer
    fields = ['id', 'answer_text', 'right']
    readonly_fields = ['id']
    extra = 1
    show_change_link = True
    can_delete = True


class QuestionAdmin(admin.ModelAdmin):
    model = Question
    inlines = [AnswerInLine]
    list_filter = ('topic',)


class AnswerAdmin(admin.ModelAdmin):
    list_display = ('question', 'answer_text', 'right')
    list_filter = ('question',)


class UserAdmin(admin.ModelAdmin):
    list_filter = ('student', 'teacher')


admin.site.register(User, UserAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Topic)
admin.site.register(Student)
admin.site.register(Subject)
admin.site.register(Group)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(StudentTopic)
admin.site.register(Teacher)
