from django_filters import FilterSet, RangeFilter, CharFilter

from .models import Topic, Question, Subject, StudentTopic, Student, Answer


class StudentTopicFilter(FilterSet):
    class Meta:
        model = StudentTopic
        fields = ('student', 'topic')


class TopicFilter(FilterSet):
    class Meta:
        model = Topic
        fields = ('name', 'subject')


class StudentFilter(FilterSet):
    class Meta:
        model = Student
        fields = ('group',)


class AnswerFilter(FilterSet):
    class Meta:
        model = Answer
        fields = ('question','right')


class QuestionFilter(FilterSet):
    class Meta:
        model = Question
        fields = ('topic',)
