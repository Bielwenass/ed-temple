from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Avg


class User(AbstractUser):
    placeholder = models.CharField(max_length=1, blank=True)

    def __str__(self):
        return f'User ID: {self.id}'


class Subject(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):
        return self.name

    def get_average_elo(self):
        topics = Topic.objects.filter(subject=self.id)
        l = len(topics)
        avg = 0
        for topic in topics:
            avg += topic.get_average_elo() / l

        return avg


class Topic(models.Model):
    name = models.CharField(max_length=128, unique=True)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}, {self.subject}'

    def get_average_elo(self):
        res = StudentTopic.objects.filter(topic=self.id).aggregate(Avg('elo'))
        return res['elo__avg']


class Question(models.Model):
    name = models.CharField(max_length=128)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    question_text = models.TextField(max_length=9999)
    elo = models.PositiveIntegerField(default=500)

    class Meta:
        ordering = ['elo']

    def __str__(self):
        return f'{self.name}, {self.topic}'


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    answer_text = models.TextField(max_length=9999)
    right = models.BooleanField(default=False)

    class Meta:
        ordering = ('question',)

    def __str__(self):
        return f'ID:{self.id}; Question: {self.question}; {self.answer_text}; Right: {self.right}'


class Group(models.Model):
    name = models.CharField(max_length=128, unique=True)
    subjects = models.ManyToManyField(Subject)

    def get_average_elo(self):
        group_students = Student.objects.filter(group=self.id)
        l = len(group_students)
        avg = 0
        for student in group_students:
            avg += student.get_average_elo() / l

        return avg


class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='teacher')


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True, related_name='student')
    group = models.ForeignKey(Group, related_name='students', on_delete=models.CASCADE)

    def get_average_elo(self):
        st = StudentTopic.objects.filter(student=self.id)
        topics = st.values_list('topic', flat=True)
        l = len(topics)
        avg = 0
        for topic in topics:
            avg += st.filter(topic=topic).first().elo / l

        return avg


class StudentTopic(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='students_topics')
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    elo = models.PositiveIntegerField(default=500)
    date_time = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("student", "topic", "date_time")
        ordering = ['-date_time']


class StudentAnswer(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now=True)
