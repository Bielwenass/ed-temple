from django.utils.six import text_type
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from .models import User, Group, Subject, Teacher, Student, Topic, StudentTopic, Answer, Question, StudentAnswer


class RegisterUserSerializer(serializers.ModelSerializer):
    """Serializer for creating user objects."""

    group = serializers.IntegerField(write_only=True)
    tokens = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'password', 'username', 'tokens', 'group')
        extra_kwargs = {'password': {'write_only': True}}

    def get_tokens(self, user):
        tokens = RefreshToken.for_user(user)
        refresh = text_type(tokens)
        access = text_type(tokens.access_token)
        data = {
            "refresh": refresh,
            "access": access
        }
        return data

    def create(self, validated_data):
        user = User(
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.Role = 3
        user.save()
        group_id = validated_data['group']
        group = Group.objects.get(id=group_id)
        student = Student(group=group, user=user)
        student.save()
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password', 'first_name', 'last_name', 'student', 'teacher']
        read_only_fields = ['id', 'student', 'teacher']
        # extra_kwargs = {'password': {'write_only': True}}
        # depth = 1


class GroupSerializer(serializers.ModelSerializer):

    def get_average_elo(self, obj):
        return obj.get_average_elo()

    average_elo = serializers.SerializerMethodField()

    class Meta:
        model = Group
        fields = ['id', 'name', 'average_elo']
        read_only_fields = ['id', 'average_elo']


class SubjectSerializer(serializers.ModelSerializer):

    def get_average_elo(self, obj):
        return obj.get_average_elo()
        
    average_elo = serializers.SerializerMethodField()

    class Meta:
        model = Subject
        fields = ['id', 'name', 'average_elo']
        read_only_fields = ['id', 'average_elo']


class TopicSerializer(serializers.ModelSerializer):

    def get_average_elo(self, obj):
        return obj.get_average_elo()
        
    average_elo = serializers.SerializerMethodField()

    class Meta:
        model = Topic
        fields = ['id', 'name', 'subject', 'average_elo']
        read_only_fields = ['id', 'average_elo']


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ['id', 'user']
        read_only_fields = ['id']


class StudentSerializer(serializers.ModelSerializer):

    def get_average_elo(self, obj):
        return obj.get_average_elo()

    average_elo = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ['id', 'group', 'user', 'average_elo']
        read_only_fields = ['id', 'average_elo']


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['id', 'question', 'answer_text', 'right']
        read_only_fields = ['id']


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ['id', 'name', 'topic', 'question_text', 'elo', ]
        read_only_fields = ['id']


class StudentTopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentTopic
        fields = ['id', 'student', 'topic', 'elo', 'date_time']
        read_only_fields = ['id', 'date_time']


class StudentAnswerSerializer(serializers.ModelSerializer):
    is_right = serializers.SerializerMethodField()

    class Meta:
        model = StudentAnswer
        fields = ['id', 'student', 'answer', 'date_time', 'is_right']

    def get_is_right(self, user, answer):
        data = {'is_right': 0}
        if answer.right:
            data = {'is_right': 1}

        return data

    def create(self, validated_data):
        user = self.context['request'].user
        validated_data.user = user
        return StudentAnswer.objects.create(**validated_data)
