import datetime

import jwt
from django.conf.global_settings import SECRET_KEY
from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework import generics, status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User, Group, Subject, Teacher, Student, Topic, StudentTopic, Answer, Question, StudentAnswer
from .serializers import (UserSerializer, GroupSerializer, SubjectSerializer, TopicSerializer,
                          TeacherSerializer, StudentSerializer, StudentTopicSerializer, AnswerSerializer,
                          QuestionSerializer, RegisterUserSerializer, StudentAnswerSerializer)

from .filters import TopicFilter, QuestionFilter, StudentTopicFilter, StudentFilter, AnswerFilter

from django.db.models import Avg


class UserTypeView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user_id = request.user.id
        user_type = 'unknown'
        related_id = 'null'
        try:
            teacher = request.user.teacher
            user_type = 'teacher'
            related_id = teacher.id
        except Teacher.DoesNotExist:
            pass
        try:
            student = request.user.student
            user_type = 'student'
            related_id = student.id
        except Student.DoesNotExist:
            pass
        return JsonResponse({'user_id': user_id, 'user_type': user_type, 'related_id': related_id})


class UserViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class UserRegistrationView(generics.CreateAPIView):
    """Handles creating Users."""
    queryset = User.objects.all()
    serializer_class = RegisterUserSerializer

    def create(self, request, *args, **kwargs):
        serializer = RegisterUserSerializer(data=request.data)
        if serializer.is_valid():
            self.perform_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GroupViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class SubjectViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer


class TopicViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    filterset_class = TopicFilter


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    filterset_class = StudentFilter


class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    filterset_class = QuestionFilter
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        queryset = self.queryset
        topic = self.request.query_params.get('topic', None)
        student = Student.objects.get(id=self.request.user.student.id)
        if topic:
            queryset = queryset.filter(topic=topic)
            use_elo = 500
            student_topic_elo_last = StudentTopic.objects.filter(student=student, topic=topic).first()
            if student_topic_elo_last:
                use_elo = student_topic_elo_last.elo
            sliced_queryset = queryset.filter(elo__gt=use_elo)[:5]
            return queryset.filter(id__in=sliced_queryset)
        return queryset

    @action(detail=True, methods=['post'])
    def submit_answer(self, request, pk=None):
        print('++++++++++++++++', request.data)
        print('----------------', request.data['answer'])
        answer = Answer.objects.get(id=request.data['answer'])

        q = self.get_object()
        topic = Topic.objects.get(id=q.topic.id)
        r_q = q.elo
        student = Student.objects.get(id=request.user.student.id)
        r_s = 500
        student_topic_elo_last = StudentTopic.objects.filter(student=student, topic=topic).first()
        if student_topic_elo_last:
            r_s = student_topic_elo_last.elo

        outcome = 1 if answer.right else 0
        coeff = 10 / Student.objects.count()
        coeff = 1 if coeff > 1 else coeff

        new_s, new_q = calc_elo(r_s, r_q, outcome, coeff_b=coeff)
        q.elo = new_q
        q.save()
        st = StudentTopic(topic=topic, elo=new_s, student=student)
        st.save()
        return Response({"ok": "ok"}, status=status.HTTP_201_CREATED)


class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    filterset_class = AnswerFilter


class StudentTopicViewSet(viewsets.ModelViewSet):
    queryset = StudentTopic.objects.all()
    serializer_class = StudentTopicSerializer
    filterset_class = StudentTopicFilter


class StudentAnswerViewSet(viewsets.ModelViewSet):
    queryset = StudentAnswer.objects.all()
    serializer_class = StudentAnswerSerializer
    permission_classes = (IsAuthenticated,)

    # def create(self, request, *args, **kwargs):
    #     serializer = RegisterUserSerializer(data=request.data)
    #     if serializer.is_valid():
    #         self.perform_create(serializer)
    #         return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def calc_elo(r_a, r_b, outcome, k=20, base=400, coeff_a=1, coeff_b=1):
    """ outcome == 1 if A won,  else 0"""
    q_a = 10 ** (r_a / base)
    q_b = 10 ** (r_b / base)

    e_a = q_a / (q_a + q_b)
    e_b = q_b / (q_a + q_b)

    return (r_a + k * (outcome - e_a) * coeff_a, r_b + k * (1 - outcome - e_b) * coeff_b)
