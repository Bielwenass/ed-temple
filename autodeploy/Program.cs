﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutodepHM3
{
    class Program
    {
        static void Main(string[] args)
            => new Program().Listen().GetAwaiter().GetResult();

        string logFile;
        string tempDir;
        string Gbranch;
        string Gdir;
        string Gdirfront;
        string CurrentMessage;
        string Notify;

        private async Task Listen()
        {
            logFile = Environment.CurrentDirectory + @"\deploy.log";
            tempDir = Environment.CurrentDirectory + @"\temp\";
            await ReadConfig();
            HttpListener listener = new HttpListener();
            listener.Prefixes.Add("http://*:9774/deploy/");
            listener.Start();
            Console.WriteLine("Awaiting connections...");

            while (true)
            {
                HttpListenerContext context = await listener.GetContextAsync();
                HttpListenerRequest request = context.Request;
                HttpListenerResponse response = context.Response;

                CurrentMessage = "";
                string text;
                using (var reader = new StreamReader(request.InputStream, request.ContentEncoding))
                {
                    text = reader.ReadToEnd();
                }
                if (text.Length > 1)
                    await DeployWork(text);

                string responseString = "<html><head><meta charset='utf8'></head><body>Привет мир!</body></html>";
                byte[] buffer = Encoding.UTF8.GetBytes(responseString);
                response.ContentLength64 = buffer.Length;
                Stream output = response.OutputStream;
                output.Write(buffer, 0, buffer.Length);
                output.Close();

            }
        }

        private async Task ReadConfig()
        {
            var confFile = await File.ReadAllTextAsync("config.json");
            var config = JObject.Parse(confFile);
            Gbranch = config["branch"].ToString();
            Gdir = config["directory"].ToString();
            Gdirfront = config["directory-front"].ToString();
            Notify = config["hook"].ToString();
        }

        private async Task SendNotification()
        {
            try
            {

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(Notify);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                string sendMessage = CurrentMessage;
                if (sendMessage.Length > 1800)
                {
                    sendMessage = sendMessage.Substring(sendMessage.Length - 1800);
                }
                var request = new
                {
                    content = "```PS\r\n" + sendMessage + "\r\n```"
                };

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = JsonConvert.SerializeObject(request);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                await Log("Error while sending a notification: " + e.Message, "ERROR");
            }
        }

        private async Task DeployWork(string request)
        {
            try
            {
                string branch = "";
                JObject o = JObject.Parse(request);

                // await Log(o.ToString());

                // Gitlab webhook is only set to fire on push to master, so this check can be omitted
                // branch = o["push"]["changes"].Last["new"]["name"].ToString();
                //if (branch != Gbranch) return;

                string commits = "";
                foreach (JObject x in o["commits"])
                {
                    commits += $@"  [{x["author"]["name"].ToString()}]: {x["message"].ToString().Replace("\n", " ")}" + Environment.NewLine;
                }

#pragma warning disable CS4014
                    Directory.SetCurrentDirectory(Gdir);
                    StartProcess("git", "reset --hard HEAD");
                    StartProcess("git", $"pull origin {Gbranch}");
                    var psiNpmRunDist = new ProcessStartInfo
                    {
                        FileName = "/bin/bash",
                        RedirectStandardInput = true,
                        WorkingDirectory = Gdirfront
                    };
                    // Directory.SetCurrentDirectory(Gdirfront);
                    // var pNpmRunDist = Process.Start(psiNpmRunDist);
                    // pNpmRunDist.StandardInput.WriteLine("npm run build & exit");
                    // pNpmRunDist.WaitForExit();
                    
                    Directory.SetCurrentDirectory(Gdir);
                    //docker-compose -f docker-compose_prod.yml up -d --build
                    // var dockerRun = Process.Start(psiNpmRunDist);
                    // dockerRun.StandardInput.WriteLine("docker-compose -f docker-compose_prod.yml up -d --build");
                    // dockerRun.WaitForExit();

                    StartProcess("docker-compose", "-f docker-compose_prod.yml up -d --build &");
                

                await Log("Deployment complete. " + Environment.NewLine + commits);

            }
            catch (Exception e)
            {
                await Log(e.Message, "ERROR");
            }
            await SendNotification();
        }

        private async Task StartProcess(string exe, string args)
        {
            Process process = new Process();
            process.StartInfo.FileName = exe;
            process.StartInfo.Arguments = args; // Note the /c command (*)
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            //* Read the output (or the error)
            string output = process.StandardOutput.ReadToEnd();
            await Log(output);
            string err = process.StandardError.ReadToEnd();
            await Log(err, "WARNING");
            process.WaitForExit();
        }

        private async Task Log(string text, string status = "INFO")
        {
            text = text.Trim();
            if (text.Length < 1) return;
            DateTime now = DateTime.Now;
            string line = $"{now.ToString("dd.MM.yyyy HH:mm:ss")} "
                + $"[{status}] {text}"
                + Environment.NewLine;
            Console.WriteLine(line);
            CurrentMessage += line;
            //await File.AppendAllTextAsync(logFile, line);
        }

    }

}

