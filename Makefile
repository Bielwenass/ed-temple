ub:
	docker-compose up --build

mm:
	docker-compose run --rm backend /bin/bash -c 'python manage.py makemigrations'

m:
	docker-compose run --rm backend /bin/bash -c 'python manage.py migrate'

ubf:
	docker-compose up --build --force-recreate

udb:
	docker-compose up -d --build

u:
	docker-compose up

d:
	docker-compose down

dv:
	docker-compose down -v

cs:
	docker-compose run --rm backend /bin/bash -c 'python manage.py collectstatic --no-input'


bash:
	docker-compose run --rm backend /bin/bash -c 'bash'

flush:
	docker-compose down
	docker system prune -a
	docker volume prune

ci:
	docker-compose -f docker-compose_ci.yml up -d --build

prodd:
	docker-compose -f docker-compose_prod.yml down

prodmm:
	docker-compose -f docker-compose_prod.yml run --rm backend /bin/bash -c 'python manage.py makemigrations --settings=backend.settings.docker'

prodm:
	docker-compose -f docker-compose_prod.yml run --rm backend /bin/bash -c 'python manage.py migrate --settings=backend.settings.docker'


prod:
	docker-compose -f docker-compose_prod.yml up -d --build