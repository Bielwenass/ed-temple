
import { User } from '@/shared/plugins/UserPlugin';

declare module 'vue/types/vue' {
  interface Vue {
    $user: User;
  }
}
