import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'
import { Role } from '@/shared/plugins/UserPlugin'

@Module({ namespaced: true })
export default class Counter2 extends VuexModule {
  _id = 0;
  _username = 'Yuri The Professional';
  _role = Role.Mentor;

  @Mutation
  auth({ id, username, role }: { id: number, username: string, role: Role }) {
    this._id = id;
    this._username = username;
    this._role = role;
  }

  get id() { return this._id; }
  get username() { return this._username; }
  get role() { return this._role; }
}
