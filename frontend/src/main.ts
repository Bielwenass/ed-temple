import 'normalize.css';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

import { AxiosPlugin } from './shared/plugins/AxiosPlugin';
Vue.use(AxiosPlugin, { baseUrl: 'http://51.158.179.130/api/' });

import { UserPlugin } from './shared/plugins/UserPlugin';
Vue.use(UserPlugin);

import HighchartsVue from 'highcharts-vue';
import Highcharts from 'highcharts';
import stockInit from 'highcharts/modules/stock';
import mapInit from 'highcharts/modules/map';

stockInit(Highcharts);
mapInit(Highcharts);

Vue.use(HighchartsVue);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
