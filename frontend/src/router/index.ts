import Vue from 'vue';
import VueRouter, { Route } from 'vue-router';
import { Role } from '@/shared/plugins/UserPlugin';

Vue.use(VueRouter);

const mentorGuard = (
  to: Route,
  from: Route,
  next: CallableFunction,
) => {
  console.log(Vue.prototype.$user);

  const role = Vue.prototype.$user.role as Role;
  console.log(`mentor guard ${role}`);
  if (role == Role.Mentor) { next(); }
  else {
    next({ name: 'tests' });
  }
};

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/user',
    component: () => import('@/views/layouts/Empty.vue'),
    children: [{
      path: 'auth',
      name: 'user.auth',
      component: () => import('@/views/Auth.vue'),
    }, {
      path: 'reg',
      name: 'user.reg',
      component: () => import('@/views/Reg.vue'),
    }],
  }, {
    path: '/',
    component: () => import('@/views/layouts/Empty.vue'),
    children: [{
      path: '/',
      name: 'home',
      component: () => import('@/views/Home.vue'),
    }, {
      path: '/test-complete',
      name: 'test-complete',
      props: true,
      component: () => import('@/views/TestComplete.vue'),
    }, {
      path: '/test',
      name: 'test',
      component: () => import('@/views/Test.vue'),
    }, {
      path: '/tests',
      name: 'tests',
      component: () => import('@/views/Tests.vue'),
    }],
  }, {
    path: '/control-panel',
    beforeEnter: mentorGuard,
    component: () => import('@/views/layouts/ControlPanel.vue'),
    children: [{
      path: '/',
      name: 'control-panel',
      component: () => import('@/components/ControlPanel/Hello.vue'),
    }, {
      path: 'groups',
      name: 'control-panel.groups',
      component: () => import('@/components/ControlPanel/Groups.vue'),
    }, {
      path: 'student',
      name: 'control-panel.student',
      component: () => import('@/components/ControlPanel/Student.vue'),
    }, {
      path: 'subjects',
      name: 'control-panel.subjects',
      component: () => import('@/components/ControlPanel/Subjects.vue'),
    }, {
      path: 'group',
      name: 'control-panel.group',
      component: () => import('@/components/ControlPanel/Group.vue'),
    }],
  }],
});

export default router;
