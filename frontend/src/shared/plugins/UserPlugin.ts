import Vue from 'vue';

export enum Role {
  Student,
  Mentor,
}

export class User {
  public id = 4;
  public username = 'Yuri The Professional';
  public role = Role.Mentor;
  public refreshTokens = '' ;
}

export function UserPlugin(vue: typeof Vue) {
  vue.prototype.$user = new User();
}
