import Vue from 'vue';
import Axios, { AxiosRequestConfig } from 'axios';

export function AxiosPlugin(
  vue: typeof Vue,
  axiosRequestConfig: AxiosRequestConfig,
) {
  const instance = Axios.create(axiosRequestConfig);
  vue.prototype.$http = instance;
  instance.defaults.baseURL = axiosRequestConfig.baseURL;
}
