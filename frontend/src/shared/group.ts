
export interface IGroup {
  id: number;
  name: string;
  average_elo: number;
}
