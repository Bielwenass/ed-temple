
export class Test {
  public id = 0;

  public text = '';
  // 0 - select
  // 1 - input
  public type = 0;

  // задание решено?
  public completed = false;

  // задание решено правильно?
  public result = false;

  public next!: Test | null;

  public answers: {
    id: number;
    answer: string;
  }[] = [];
}
